(function (exports) {
    const DIR_ACROSS = 1;
    const DIR_DOWN = 2;

    const WORDS_POSITIONS = new Map();

    function charOffsets(word, char) {
        const offsets = [];

        for (let offset = 0; offset < word.length; offset++) {
            if (word[offset] === char) {
                offsets.push(offset);
            }
        }

        return offsets;
    }

    function storeWordPosition(word, sx, sy, direction) {
        if (direction === DIR_ACROSS) {
            WORDS_POSITIONS.set(word, {
                sx,
                sy,
                ex: sx + word.length - 1,
                ey: sy,
                direction
            });
        } else {
            WORDS_POSITIONS.set(word, {
                sx,
                sy,
                ex: sx,
                ey: sy + word.length - 1,
                direction
            });
        }
    }

    function clearWordPosition(word) {
        if (WORDS_POSITIONS.has(word)) {
            WORDS_POSITIONS.delete(word);
        }
    }

    function charAt(word, x, y) {
        let offset;
        const coordinates = WORDS_POSITIONS.get(word);

        if (coordinates.direction === DIR_DOWN) {
            offset = Math.abs(coordinates.sy) + y;
        } else {
            offset = Math.abs(coordinates.sx) + x;
        }

        return word[offset];
    }

    function charPos(coordinates, offset) {
        if (coordinates.direction === DIR_DOWN) {
            return {
                x: coordinates.sx,
                y: coordinates.sy + offset
            };
        }

        return {
            x: coordinates.sx + offset,
            y: coordinates.sy
        };
    }

    function isWithinRange(point, start, end) {
        return point >= start && point <= end;
    }

    function hasCollisions(word, offset, x, y, direction) {
        const endX = direction === DIR_ACROSS ? x + word.length - 1 : x;
        const endY = direction === DIR_DOWN ? y + word.length - 1 : y;
        const coords = {
            sx: x,
            sy: y,
            ex: endX,
            ey: endY,
            direction
        };
        const char = word[offset];
        const pos = charPos(coords, offset);

        if (direction === DIR_ACROSS) {
            for (const [word, coords] of WORDS_POSITIONS) {
                if (coords.direction !== DIR_DOWN || !isWithinRange(pos.y, coords.sy, coords.ey)) {
                    continue;
                }

                if (charAt(word, pos.x, pos.y) !== char) {
                    return true;
                }
            }
        } else {
            for (const [word, coords] of WORDS_POSITIONS) {
                if (coords.direction !== DIR_ACROSS || !isWithinRange(pos.x, coords.sx, coords.ex)) {
                    continue;
                }

                if (charAt(word, pos.x, pos.y) !== char) {
                    return true;
                }
            }
        }

        return false;
    }

    function noMeaninglessNeighbours(word, x, y, direction) {
        const endX = direction === DIR_ACROSS ? x + word.length - 1 : x;
        const endY = direction === DIR_DOWN ? y + word.length - 1 : y;

        if (direction === DIR_ACROSS) {
            for (const [candidate, coords] of WORDS_POSITIONS) {
                if (coords.direction === DIR_ACROSS) {
                    if (coords.sy > y + 1 || coords.ey < y - 1) {
                        continue;
                    }

                    // TODO: Perform checks for meaningless neighbours

                    continue;
                }

                if (coords.sx > x + 1 || coords.ex < x - 1) {
                    continue;
                }

                if (coords.sx === endX + 1 || coords.ex === x - 1) {
                    return false;
                }
            }
        } else {
            for (const [candidate, coords] of WORDS_POSITIONS) {
                if (coords.direction === DIR_ACROSS) {
                    if (coords.sx > x + 1 || coords.ex < x - 1) {
                        continue;
                    }

                    // TODO: Perform checks for meaningless neighbours

                    continue;
                }

                if (coords.sy > y + 1 || coords.ey < y - 1) {
                    continue;
                }

                if (coords.sy === endY + 1 || coords.ey === y - 1) {
                    return false;
                }
            }
        }

        return true;
    }

    function canPlaceWord(word, offset, x, y, direction) {
        return !hasCollisions(word, offset, x, y, direction) && noMeaninglessNeighbours(word, x, y, direction);
    }

    function crosswords(word, cross, wordOffset, direction) {
        if (WORDS_POSITIONS.size === 0) {
            storeWordPosition(word, 0, 0, direction);
        }

        const wordCoordinates = WORDS_POSITIONS.get(word);

        if ((direction === DIR_ACROSS && wordCoordinates.ex > wordCoordinates.sx) || (
            direction === DIR_DOWN && wordCoordinates.ey > wordCoordinates.sy)) {
            // Words with same direction and overlapping characters couldn't be crossed.
            return false;
        }

        const crossCharOffsets = charOffsets(cross, word[wordOffset]);

        for (const offset of crossCharOffsets) {
            let x, y;

            if (direction === DIR_ACROSS) {
                x = wordCoordinates.sx - offset;
                y = wordCoordinates.sy + wordOffset;
            } else {
                x = wordCoordinates.sx + wordOffset;
                y = wordCoordinates.sy - offset;
            }

            if (canPlaceWord(cross, offset, x, y, direction)) {
                storeWordPosition(cross, x, y, direction);
                return true;
            }
        }

        return false;
    }

    function wordsContaingChar(words, char) {
        return words.filter((word) => {
            return word.indexOf(char) > -1;
        });
    }

    function generateCrosswords(words, next = null) {
        let triedWords = 0;

        if (words.length === 0) {
            return true;
        }

        while (words.length > 0) {
            const word = next || words.shift();
            const length = word.length;

            for (let offset = 0; offset < length; offset++) {
                const char = word[offset];

                let nextTryWith;

                for (const cross of wordsContaingChar(words, char)) {
                    let crossed = crosswords(word, cross, offset, DIR_ACROSS);

                    if (!crossed && crosswords(word, cross, offset, DIR_DOWN)) {
                        crossed = true;
                    }

                    if (crossed) {
                        nextTryWith = cross;
                        break;
                    }
                }

                if (nextTryWith) {
                    words.splice(words.indexOf(nextTryWith), 1);

                    if (!generateCrosswords(words, nextTryWith)) {
                        words.push(nextTryWith);
                        clearWordPosition(nextTryWith);
                        continue;
                    }

                    return true;
                }
            }

            if (next === null) {
                words.push(word);
                clearWordPosition(word);
            }

            if (++triedWords === words.length) {
                return false;
            }
        }

        return false;
    }

    exports.crosswordsGenerator = {
        DIR_ACROSS, DIR_DOWN,
        generate: function (words) {
            const wordsToUse = words.map(word => word.toLowerCase()).filter(word => word.replace(/^\s+|\s$/, '').length > 0);

            if (generateCrosswords(wordsToUse)) {
                return WORDS_POSITIONS;
            }

            return null;
        }
    };
})(typeof module === "object" && module.exports ? module.exports : window);